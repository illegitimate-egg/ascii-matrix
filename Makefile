CC=gcc
CFLAGS=-Wall

SRC=ascii-matrix.c
EXE=ascii-matrix

.PHONY: all
all:$(SRC)
	$(CC) $(SRC) $(CFALGS) -o $(EXE)

.PHONY: clean
clean:
	rm -rf $(EXE)
